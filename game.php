<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Escape game</title>

        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

        <link rel="stylesheet" href="game.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    </head>
    <body>

        <?php
            if(isset($_POST['pseudo'])){
                if(!empty($_POST['pseudo'])){
                    echo "<div id='pseudo' style='display:none'>".$_POST['pseudo']."</div>";
                }else{
                    header('Location: index.php?msg=error1');
                }
            }else{
                header('Location: index.php?msg=error2');
            }
        ?>

        <header><h4>Sherlock est dans la place !</h4></header>
        <div class='personnages'>
            <div id="p_1"><img src='data/personnages/carte.png' alt='fond carte perso' style='height: 100%; width: 100%;object-fit: contain'></div>
            <div id="p_2"><img src='data/personnages/carte.png' alt='fond carte perso' style='height: 100%; width: 100%;object-fit: contain'></div>
            <div id="p_3"><img src='data/personnages/carte.png' alt='fond carte perso' style='height: 100%; width: 100%;object-fit: contain'></div>
            <div id="p_4"><img src='data/personnages/carte.png' alt='fond carte perso' style='height: 100%; width: 100%;object-fit: contain'></div>
            <div id="p_5"><img src='data/personnages/carte.png' alt='fond carte perso' style='height: 100%; width: 100%;object-fit: contain'></div>
            <div id="p_6"><img src='data/personnages/carte.png' alt='fond carte perso' style='height: 100%; width: 100%;object-fit: contain'></div>
            <div id="p_7"><img src='data/personnages/carte.png' alt='fond carte perso' style='height: 100%; width: 100%;object-fit: contain'></div>
            <div id="p_8"><img src='data/personnages/carte.png' alt='fond carte perso' style='height: 100%; width: 100%;object-fit: contain'></div>
        </div>

        <div class="colonneGauche">
            <div class='messages' id='message' ></div>

            <div class='boussole' id ='divBoussole'></div>

            <div class='temps'>
              <p id='message_temps'>Il vous reste :</p>
              <p id='chrono'>20:00</p>
            </div>
            <div class='meilleursScores'>
              <p id='txtSc'>Voici les meilleurs scores :</p>
              <p id='sc1'></p>
              <p id='sc2'></p>
              <p id='sc3'></p>
            </div>

        </div>

        <div class="colonneDroite">
          <div class='outils' id='outil'> Ressources : <br></div>
          <div class='notes'>Notes personnelles
            <textarea id="notesPerso"></textarea>
          </div>

            <div class='toutIndices'>Indices : <br></div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="modalGame" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLabel"></h5>
                    <button id="boutonclose" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-content-div"></div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
            </div>
        </div>


        <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

        <div class='Carte'>
            <iframe src="carte.html" width="100%" height="99%"></iframe>
        </div>

    <footer>Escape Game réalisé par Iris Jeuffrard & Mathilde Waymel - Novembre 2020</footer>

    </body>
</html>
