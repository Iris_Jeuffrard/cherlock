# Procédure d'installation

## Environnement : 

Nous avons développé et déployé l’application WEB sur une machine tournant sous Debian 10 et une seconde sous Mac-OS. 

## Procédure d’installation Linux : 

Tout d’abord il vous faut installer une stack Lamp qui comprend : 
* Le serveur web Apache (version /2.4.38 (Debian))
* Le serveur de base de données MariaDB (version 10.3.27)  et phpMyAdmin (version 4.9.7) 
* Le langage de script PHP (version 7.3.19) et quelques extension utiles (Extension PHP : mysqli, curl, mbstring) 

Cf tuto : [https://www.linuxtricks.fr/wiki/debian-installer-un-serveur-lamp-apache-mysql-php](https://www.linuxtricks.fr/wiki/debian-installer-un-serveur-lamp-apache-mysql-php) 

Ensuite copier le projet à la racine du serveur web à savoir : `/var/www/html` 

## Procédure d’installation Mac-OS : 

Tout d’abord il vous faut installer une stack Mamp qui comprend :
* Le serveur web Apache (version /2.4.46 (Unix)) 
* Le serveur de base de données MySGL (version 5.7.30) et phpMyAdmin (version 4.9.5) 
* Le langage de script PHP (version 7.4.9) et quelques extension utiles (Extension PHP : mysqli, curl, mbstring) 

Se rendre sur le site [(https://www.mamp.info/en/mamp/mac/)](https://www.mamp.info/en/mamp/mac/) et télécharger la version gratuite. 

Vérifier que le Web server sélectionné est Apache puis lancer en appuyant sur Start (en haut à droite). 

Enfin, copier le projet à la racine du serveur web comme indiqué par “Document root” (par défaut `/Application/MAMP/htdocs`). 

## Modifications préalables : 

--> Importer la bdd “Enquete.sql” depuis la page [http://localhost/phpmyadmin/](http://localhost/phpmyadmin/) en créant une bdd puis en cliquant sur “importer”,” parcourir” (glisser le ficher .sql), “exécuter”. 

--> Adapter dans le fichier “connect.php” les lignes suivante en fonction de vos paramètres de connexion à votre base de données : 

```php
$user = 'votre username'; 
$password = 'votre mdp'; 
$db = 'Enquete'; 
$host = 'localhost'; 
$port = 3306; 
```

## Lancer le jeu : 

Pour accéder à la page de l’escape game, se rendre sur cette page :  [http://localhost/index.php](http://localhost/index.php)

**/!\\** Nous rencontrons parfois des bugs liés au chargement Leaflet (certains marker ou event ne sont plus chargés) dans ce cas fermer et ré-ouvrir la fenêtre. De manière générale, privilégier chrome si possible. 

**Rq** : Nous avons utilisé boostrap (version 4.5.3) et jquery (version 3.5.1), les liens js et css sont directement inclus dans les fichiers.
