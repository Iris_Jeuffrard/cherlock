<?php
    
    include('connect.php');

    if(isset($_POST["initLieu"])){

        $sql = "SELECT * FROM `Lieux` WHERE charge = 1";
        $result = mysqli_query($link, $sql);

        if($result){
            $lieuCharge = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $lieuCharge[] = $ligne;
            }
        }
        echo json_encode($lieuCharge);

    }elseif(isset($_POST["initMessage"])){
        $sql = "SELECT * FROM `Message` WHERE charge = 1";
        $result = mysqli_query($link, $sql);

        if($result){
            $MessCharge = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $MessCharge[] = $ligne;
            }
        }
        echo json_encode($MessCharge);

    }elseif(isset($_POST["initOutil"])){
        $sql = "SELECT * FROM `Outils` WHERE charge = 1";
        $result = mysqli_query($link, $sql);

        if($result){
            $OutilsCharge = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $OutilsCharge[] = $ligne;
            }
        }
        echo json_encode($OutilsCharge);

    }elseif(isset($_POST["bloqueur"])){
        $sql = "SELECT * FROM `Lieux` WHERE `estdebloquepar`=".$_POST["bloqueur"];
        $result = mysqli_query($link, $sql);

        if($result){
            $lieuDebloque = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $lieuDebloque[] = $ligne;
            }
        }
        echo json_encode($lieuDebloque);

    }elseif(isset($_POST["idimage"])){
        $sql = "SELECT * FROM `Images` WHERE `imid`=".$_POST["idimage"];
        $result = mysqli_query($link, $sql);

        if($result){
            $image = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $image[] = $ligne;
            }
        }
        echo json_encode($image);

    }elseif(isset($_POST["idExplication"])){
        $sql = "SELECT * FROM `Explications` WHERE `eid`=".$_POST["idExplication"];
        $result = mysqli_query($link, $sql);

        if($result){
            $explication = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $explication[] = $ligne;
            }
        }
        echo json_encode($explication);

    }elseif(isset($_POST["explicationContenantObjet"])){
        $sql = "SELECT * FROM `Objets` WHERE `explication`=".$_POST["explicationContenantObjet"];
        $result = mysqli_query($link, $sql);

        if($result){
            $objet = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $objet[] = $ligne;
            }
        }
        echo json_encode($objet);

    }elseif(isset($_POST["explicationContenantPerso"])){
        $sql = "SELECT * FROM `Personnages` WHERE `explication`=".$_POST["explicationContenantPerso"];
        $result = mysqli_query($link, $sql);

        if($result){
            $perso = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $perso[] = $ligne;
            }
        }
        echo json_encode($perso);

    }elseif(isset($_POST["lieuDebloquantFichePerso"])){
        $sql = "SELECT pid,p_nom,iconemap,imagefiche FROM `Personnages` WHERE `fichedebloquepar`=".$_POST["lieuDebloquantFichePerso"];
        $result = mysqli_query($link, $sql);

        if($result){
            $fiches = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $fiches[] = $ligne;
            }
        }
        echo json_encode($fiches);

    }elseif(isset($_POST["lieuDebloquantMessage"])){
        $sql = "SELECT * FROM `Message` WHERE `estdebloquepar` =".$_POST["lieuDebloquantMessage"];
        $result = mysqli_query($link, $sql);

        if($result){
            $Message = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $Message[] = $ligne;
            }
        }
        echo json_encode($Message);

    }elseif(isset($_POST["lieuDebloquantOutil"])){
        $sql = "SELECT * FROM `Outils` WHERE `estdebloquepar` =".$_POST["lieuDebloquantOutil"];
        $result = mysqli_query($link, $sql);

        if($result){
            $Outils = [];
            $output = "";
            while($ligne = mysqli_fetch_assoc($result)){
            $Outils[] = $ligne;
            }
        }
        echo json_encode($Outils);

    }elseif(isset($_POST["code"])){
        if($_POST["code"]=='londonaz'){
            echo json_encode(1);
        }else{echo json_encode (0);}

    }
?>
