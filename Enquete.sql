-- phpMyAdmin SQL Dump
-- version 4.9.7deb1~bpo10+1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 18 déc. 2020 à 21:34
-- Version du serveur :  10.3.27-MariaDB-0+deb10u1
-- Version de PHP : 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `Enquete`
--

-- --------------------------------------------------------

--
-- Structure de la table `Explications`
--

CREATE TABLE `Explications` (
  `eid` int(11) NOT NULL,
  `e_nom` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  `fin` tinyint(1) NOT NULL DEFAULT 1,
  `esuivante` int(11) DEFAULT NULL,
  `displayed` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Explications`
--

INSERT INTO `Explications` (`eid`, `e_nom`, `content`, `fin`, `esuivante`, `displayed`) VALUES
(1, ' ', '<img src=\'data/personnages/icone0.png\' alt=\'IconeSherlock\' width=\'50\' height=\'50\'>\r\n<b>Bonjour Sherlock Holmes</b> <br>\r\nVous êtes chez vous au 221B Baker Street. <br>\r\nRegardez, vous venez de recevoir un mail !  \r\nC’est Sebastian, votre vieil ami qui dirige une banque, il vous demande de l’aide... <br>\r\nN\'oublie pas de <b>cliquer sur l\'icone</b> des indices pour les voir (après il sera trop tard...) <br>\r\n<b>⬇⬇⬇</b>\r\n', 1, NULL, 0),
(2, '>>', '<b>Bank of England</b><br>\r\nSebastian vous informe d’une effraction dans le bureau, situé au 1er étage, de M. Edward Van Coon, le chef du département des affaires de Hong Kong. \r\n', 0, 3, 0),
(3, '>>', 'Aucun objet ne manque à l’appel, et rien ne semble avoir été abimé... Seule trace du mystérieux visiteur : de curieux signes peints sur le mur.  <br>\r\nN\'oublie pas de <b>cliquer sur l\'icone</b> des indices pour les voir (certains sont récupérables comme celui-çi) <br>\r\n<b>⬇⬇⬇</b>\r\n', 0, 4, 0),
(4, '>>', 'A 23h34, le 17 décembre, le mur était peint, pourtant la porte était verrouillée et aucun des systèmes de surveillance aux entrées de la banque n’a détecté le moindre passage : comment a-t-il pu parvenir au bureau tagué sans laisser de traces ?!  \r\n', 1, NULL, 0),
(5, '>>', '<b>Appartement d\'Edward Van Coon</b>  <br>\r\nIl semble effectivement indispensable de rendre visite à M. Van Coon, peut-être saura-t-il quelque chose... Son appartement est verrouillé de l’intérieur, il est donc présent.  <br>\r\nPourtant il ne vient pas vous ouvrir !! Il faut absolument rentrer... \r\n\r\n', 0, 6, 0),
(6, '>>', '<p>Fais le bon choix pour pouvoir continuer ;)</p>\r\n<p>\r\n<button id=\"escalader\" type=\"button\" class=\"btn btn-secondary btn-sm\"onclick=\"btnEscalader(event)\">\r\nEscalader le balcon\r\n</button>\r\n<button id=\"abandonner\" type=\"button\" class=\"btn btn-secondary btn-sm\"onclick=\"btnAbandonner(event)\">\r\nBaisser les bras\r\n</button>\r\n</p>\r\n<p id =\"commentaire1\"></p>', 0, 7, 0),
(7, '>>', 'Escalader le balcon des voisins pour parvenir dans l’appartement verrouillé était une excellente idée !  <br>\r\n\r\nM. Van Coon est bien là, mais il est mort ! Comment l’agresseur serait-il rentré ? Ou bien est-ce un suicide ? <br>\r\n\r\nLes apparences sont trompeuses, et cette scène vous rappelle les peintures de la banque...  <br>\r\n\r\nMais qui est donc ce M. Van Coon ? Qui pourrait lui vouloir sa mort ? Et pourquoi ? \r\n<p><button type=\"button\" class=\"btn btn-warning btn-sm\" data-toggle=\"modal\" data-target=\"#modalCarte\">\r\n  Lire le journal du jour\r\n</button></p>', 1, NULL, 1),
(8, '>>', '<b>Appartement de Brian Lukis (4ème étage)</b> <br>\r\nRien à signaler chez Brian Lukis hormis ce billet que vous avez trouvé dans son agenda...<br> Votre enquête piétine et pendant ce temps l’assassin continue de tuer !\r\n', 0, 9, 0),
(9, '>>', 'Au moment de ressortir, vous apercevez cependant un livre par terre. Vous le ramassez. \r\nIl a été tamponné à la Westminster Reference Library le jour de sa mort... ', 1, NULL, 0),
(10, ' ', '<b>Westminster Reference Library</b><br>\r\nAu fond du rayonnage où se trouvait le livre, vous découvrez un second message peint... <br>\r\nM. Lukis a dû le voir peu avant de retourner chez lui.  <br>\r\nMais quel est donc le lien entre M. Van Coon et Brian Lukis ? Que signifie ce message ? Et qui sont-ils ? ', 1, NULL, 0),
(11, ' ', '<b>Bank of England</b><br>\r\nAmanda, la secrétaire de M. Van Coon vous est d’une aide précieuse : elle vous confie de quoi éclaircir l’emploi du temps de la victime.', 1, NULL, 0),
(12, '>>', '<b>The Lucky Cat</b><br>\r\nPourquoi M. Van Coon a-t-il dans son agenda l’adresse de cette boutique chinoise ?! Est-ce en lien avec son voyage à Pékin ? <br>\r\nVous faits le tour de la boutique et jetez un coup d’œil aux objets. <br>\r\nVous repérez une étiquette sur un objet : elle est couverte des mêmes symboles que les tags du bureau de M. Van Coon ! ', 0, 13, 0),
(13, '>>', 'Il s’agit en fait d’une étiquette indiquant le prix, les symboles sont donc des chiffres ! <br>\r\nEn cherchant sur un site de vente aux enchères, vous retrouvez cet article datant du 16 décembre, le lendemain du retour de Chine de M. Van Coon. <br>\r\nBizarrement la vente a été faite sous un nom anonyme...  <br>\r\nVous réfléchissez...', 0, 14, 0),
(14, '>>', '<p>➤ Qui sont M Van Coon et Brian Lukis ? <br>\r\n            <label><input type=\"radio\" name=\"Q1\" value=\"0\"> Acheteurs</label>\r\n            <label><input type=\"radio\" name=\"Q1\" value=\"0\"> Enquêteurs</label>\r\n            <label><input type=\"radio\" name=\"Q1\" value=\"1\"> Trafiquants</label>\r\n</p>  \r\n<p>➤ Quelle est la fonction du Lucky Cat ? <br>\r\n            <label><input type=\"radio\" name=\"Q2\" value=\"0\"> Lieu de vente au marché noir</label><br>\r\n            <label><input type=\"radio\" name=\"Q2\" value=\"1\"> Lieu de couverture pour déposer des marchandises volées</label><br>\r\n            <label><input type=\"radio\" name=\"Q2\" value=\"0\"> Simple boutique de souvenirs</label> \r\n</p>  \r\n<p>➤ Pourquoi ont-ils été tués ? <br>\r\n            <label><input type=\"radio\" name=\"Q3\" value=\"0\"> Ils ont été trop curieux </label><br>\r\n            <label><input type=\"radio\" name=\"Q3\" value=\"0\"> Ils ont refusé de payer</label><br>\r\n            <label><input type=\"radio\" name=\"Q3\" value=\"1\"> Ils ont volé des marchandises </label>\r\n</p>\r\n<button id=\"corriger\" type=\"button\" class=\"btn btn-secondary btn-sm\" onclick=\"btnCorriger(event)\">Corriger</button>\r\n<p id=\"correction\"> </p>', 0, 15, 0),
(15, '>>', 'Vous êtes très perspicace ! <br>\r\nLes deux hommes venaient de rentrer de Chine. Ils sont membres d’un éminent réseau de trafiquants : le Lotus Noir. Lors de leur dernière mission ils ont discrètement ramené à Londres des antiquités, dérobées par le Lotus Noir.\r\nPour ensuite déposer la marchandise au Lucky Cat qui sert de lieu de couverture. <br>\r\nLe lotus noir s’est alors rendu compte qu’une marchandise précieuse, une barrette de jade appartenant à l’impératrice Wu Zetian, manquait à l’appel. \r\nIl a demandé aux deux hommes que celui qui l’avait subtilisé la lui rende, ou il les tuerait. \r\nEn l’absence de réponse, ils ont tous les deux été assassinés... \r\n<br>\r\nMaintenant il faut retrouver les trafiquants et les arrêter ! ', 1, 16, 1),
(16, '>>', '<b>Appartement de Soo Lin Yao</b><br>\r\nEn ressortant de la boutique, votre attention a été attirée par cet appartement : il semble abandonné par son locataire et pourtant vous êtes sûr d’y avoir vu un éclat de lumière... ', 0, 17, 0),
(17, '>>', '<p>Evidemment c’est fermé, que faites-vous ? </p>\r\n<button id=\"repartir\" type=\"button\" class=\"btn btn-secondary btn-sm btn-block\" onclick=\"btnRepartir(event)\">Tant pis, je fais demi-tour</button>\r\n<button id=\"attendre\" type=\"button\" class=\"btn btn-secondary btn-sm btn-block\" onclick=\"btnAttendre(event)\">J\'attends quelques instants un miracle</button>\r\n<button id=\"fenetre\" type=\"button\" class=\"btn btn-secondary btn-sm btn-block\" onclick=\"btnFenetre(event)\">Je dévoile mes talents de sportif et j\'entre par la fenêtre</button>\r\n<p id=\"commentaire\"></p> ', 0, 18, 0),
(18, '>>', '<img src=\"data/lieux/pow.png\" alt=\'image boom\' style=\'height: 100%; width: 100%; object-fit: contain\'>', 0, 19, 1),
(19, '>>', 'Ahouch !! Vous reprenez connaissance, étendu au sol. <br>\r\nQue s’est-il passé ? Ah oui, vous avez échappé de justesse à un mystérieux individu acrobate introduit dans l’appart par effraction (comme vous d’ailleurs), il vous a assommé par surprise ! <br>\r\nVous vous en sortez avec seulement quelques égratignures, ouf. Vous vous relevez douloureusement et vérifiez qu’il ne vous a rien volé. <br>\r\nVous trouvez dans votre poche une fleur de lotus noir ! ', 0, 20, 1),
(20, '>>', 'Vous faites le tour de l’appartement avant de ressortir. \r\n\r\nIl s’agit de l’appartement de Soo Lin Yao, elle semble travailler au National Museum Antiquity.', 0, 21, 1),
(21, '>>', 'Vous ressortez de l’appartement (toujours par la fenêtre) et décidez de vous rendre au National Museum Antiquity. En chemin vous rencontrez Tom. <br>\r\nIl s’agit d’un jeune homme graffeur très perspicace et qui vous a déjà aidé dans vos enquêtes. Vous décidez de lui raconter toute cette histoire, peut-être pourra-t-il vous aidez à y voir plus clair. <br>\r\nAlors que vous lui décrivez les peintures jaunes, son regard s’illumine... <br>\r\nIl vous mène à un mur tagué qu’il a découvert il y a peu. ', 1, NULL, 1),
(22, '>>', '<b>The British Museum </b><br>\r\nVous vous rendez ensuite au musée, vous y retrouvez enfin Soo Lin Yao.  <br>\r\nElle se méfie de vous mais vous finissez par la convaincre de vous parler. <br>\r\nElle vous apprend alors qu’elle veut faire table rase sur son passé de trafiquante pour le Lotus Noir : un syndicat de crime très en ancien de Chine.  <br>\r\nElle essaye particulièrement d’échapper à son frère Zhi Zhu ‘l’homme araignée’ enrôlé et aveuglé par le Lotus Noir et son général Shan.', 0, 23, 0),
(23, '>>', 'Vous l’interrogez à propos des chiffres peints. Elle est impressionnée par vos découvertes et vous explique que c’est un système de codage assez simple : il est basé sur un livre que l’expéditeur et le destinataire du message possèdent. <br>\r\n\r\nLe message est constitué de plusieurs paires de nombres et chaque paire code pour un mot : le premier nombre correspond à un numéro de page et le second au numéro du mot dans la page. Par exemple 15-1 fait référence au 1er mot de la page 15. ', 0, 24, 0),
(24, '>>', 'Vous avancez beaucoup ! Malheureusement, elle n’en sait pas plus... Quel livre cela peut-il être ?! <br>\r\nAh si, avant de partir elle vous donne quand même un papier avec un message codé : elle pense que c’est le titre du livre mais elle n’a pas eu de clé pour le décoder... <br>\r\nA vous de jouer ! ', 0, 25, 0),
(25, '>>', '<p>Saisis ici le message décodé :</p>\r\n<form method=\"post\" action=\"php/bdd.php\">\r\n<input type=\"text\" name=\"code\" id=\"code\">\r\n</form>\r\n<button id=\"verifier\" type=\"button\" class=\"btn btn-secondary btn-sm\" onclick=\"btnVerifier(event)\">Vérifier</button>\r\n<p id=\"resultat\"></p>', 0, 26, 0),
(26, '>>', 'Bien joué !! Vous vous procurez le livre, une fois décodé le message donne : <br>\r\nCode décodé : NEUF MILLION POUR BARRETTE JADE RDV UNDERGROUND CHATEAU ELEPHANT <br>\r\nMaintenant, à vous de trouver le repère de ces trafiquants de d’arrêter l’assassin ! ', 1, NULL, 1),
(27, ' ', '<b>Station Elephant&Castle</b><br>\r\nBravo vous avez trouvé le repère des trafiquants et l’assassin Zhi Zhu, quelle efficacité ! ', 1, NULL, 0),
(28, 'News du Jour !', NULL, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `Images`
--

CREATE TABLE `Images` (
  `imid` int(11) NOT NULL,
  `chemin` text NOT NULL,
  `largeur` int(11) NOT NULL,
  `hauteur` int(11) NOT NULL,
  `ancre_x` int(11) NOT NULL,
  `ancre_y` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Images`
--

INSERT INTO `Images` (`imid`, `chemin`, `largeur`, `hauteur`, `ancre_x`, `ancre_y`) VALUES
(1, 'data/lieux/icone1.png', 36, 28, 18, 14),
(2, 'data/lieux/icone2.png', 28, 28, 14, 14),
(3, 'data/lieux/icone3.png', 36, 28, 18, 14),
(4, 'data/lieux/icone5.png', 36, 28, 18, 14),
(5, 'data/lieux/icone6.png', 26, 28, 13, 14),
(6, 'data/lieux/icone8.png', 20, 28, 10, 14),
(7, 'data/lieux/icone9.png', 36, 28, 18, 14),
(8, 'data/lieux/icone11.png', 34, 28, 17, 14),
(9, 'data/lieux/icone10.png\r\n', 35, 28, 17, 14);

-- --------------------------------------------------------

--
-- Structure de la table `Joueurs`
--

CREATE TABLE `Joueurs` (
  `jid` int(11) NOT NULL,
  `pseudo` text DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `temps` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Joueurs`
--

INSERT INTO `Joueurs` (`jid`, `pseudo`, `score`, `temps`) VALUES
(37, 'Riri', 748, '7:42'),
(38, 'Fifi', 445, '12:45'),
(39, 'Loulou', 177, '15:13');

-- --------------------------------------------------------

--
-- Structure de la table `Lieux`
--

CREATE TABLE `Lieux` (
  `lid` int(11) NOT NULL,
  `l_nom` text DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `icone` int(11) DEFAULT NULL,
  `explication` int(11) DEFAULT NULL,
  `zoom` int(11) DEFAULT NULL,
  `charge` tinyint(1) NOT NULL DEFAULT 0,
  `estdebloquepar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Lieux`
--

INSERT INTO `Lieux` (`lid`, `l_nom`, `latitude`, `longitude`, `icone`, `explication`, `zoom`, `charge`, `estdebloquepar`) VALUES
(1, '221B Baker Street', 51.523778, -0.158506, 1, 1, 10, 1, NULL),
(2, 'Bank of England', 51.514214, -0.088424, 2, 2, 15, 0, 1),
(3, 'Appartement d\'Edward Van Coon', 51.496493, -0.127091, 3, 5, 15, 0, 2),
(4, 'Maison de la presse', NULL, NULL, NULL, 28, NULL, 0, 3),
(5, 'Appartement de Brian Lukis', 51.512977, -0.063144, 4, 8, 15, 0, 4),
(6, 'Westminster Reference Library', 51.509623, -0.129824, 5, 10, 15, 0, 5),
(7, 'Bank of England', 51.514214, -0.088424, 2, 11, 15, 0, 6),
(8, 'The Lucky Cat', 51.511249, -0.132011, 6, 12, 17, 0, 7),
(9, 'Appartement de Soo Lin Yao', 51.511149, -0.132464, 7, 16, 15, 0, 8),
(10, 'The British Museum', 51.51951, -0.127035, 8, 22, 14, 0, 9),
(11, 'Station Traficant', 51.49462, -0.100518, 9, 27, 15, 0, 10);

-- --------------------------------------------------------

--
-- Structure de la table `Message`
--

CREATE TABLE `Message` (
  `mid` int(11) NOT NULL,
  `titre` text NOT NULL,
  `message` text NOT NULL,
  `charge` tinyint(1) NOT NULL,
  `estdebloquepar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Message`
--

INSERT INTO `Message` (`mid`, `titre`, `message`, `charge`, `estdebloquepar`) VALUES
(1, 'Init', '<br>Te voilà dans la peau de Sherlock Holmes. Résous l\'énigme en te déplaçant de lieu en lieu sur la carte\r\n<br>Que l\'aventure commence !', 1, NULL),
(2, 'Aide indice', '<br>Clique sur l\'indice pour le voir ! \r\n<br> Et n\'hésite pas à cliquer sur la boussole, elle te donnera la direction du lieu que tu recherches !', 0, 1),
(3, 'Aide fiche&boussole', '<br>Regarde plus haut la biographie des personnages apparaît ! Cela peut-être très utile...\r\n\r\n', 0, 2),
(4, 'Aide librairie', '<br>Vous devriez peut-être aller interroger la secrétaire de la banque, elle dispose de l’emploi du temps de M. Van Coon...', 0, 6),
(5, 'Aide appart', '<br>En ressortant du Lucky Cat, vous avez aperçu quelque chose...', 0, 8),
(6, 'aide decode', '<br>Le saviez-vous ? <br>\r\nVos ressources sont d\'une grande utilité dans cette enquête ! :p', 0, 10),
(9, 'Aide Lucky Cat', '<br>Picadilly Circus... Mmmm ça ressemble à une station ça... C\'est peut-être sur le plan ?', 0, 7);

-- --------------------------------------------------------

--
-- Structure de la table `Objets`
--

CREATE TABLE `Objets` (
  `oid` int(11) NOT NULL,
  `o_nom` text NOT NULL,
  `iconemap` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `charge` tinyint(1) NOT NULL DEFAULT 0,
  `recuperable` tinyint(1) NOT NULL DEFAULT 0,
  `explication` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Objets`
--

INSERT INTO `Objets` (`oid`, `o_nom`, `iconemap`, `image`, `charge`, `recuperable`, `explication`) VALUES
(1, 'Mail', 'data/objets/icone1.png', 'data/objets/indice1.png', 1, 0, 1),
(2, 'Message crypté 1', 'data/objets/icone0.png', 'data/objets/indice2.png', 0, 1, 3),
(3, 'Images surveillance', 'data/objets/icone3.png', 'data/objets/indice3.png', 0, 1, 4),
(4, 'News', 'data/objets/icone0.png', 'data/objets/indice4.png', 0, 0, 28),
(5, 'Livre', 'data/objets/icone5.png', 'data/objets/indice5.png', 0, 0, 9),
(6, 'AgendaL', 'data/objets/icone6.png', 'data/objets/indice6.png', 0, 1, 8),
(7, 'Message crypté 2', 'data/objets/icone0.png', 'data/objets/indice7.png', 0, 1, 10),
(8, 'AgendaVC', 'data/objets/icone8.png', 'data/objets/indice8.png', 0, 1, 11),
(9, 'Trajet', 'data/objets/icone9.png', 'data/objets/indice9.png', 0, 0, 11),
(10, 'Etiquette', 'data/objets/icone10.png', 'data/objets/indice10.png', 0, 0, 12),
(11, 'Enchères', 'data/objets/icone11.png', 'data/objets/indice11.png', 0, 1, 13),
(12, 'Adresse musée', 'data/objets/icone12.png', 'data/objets/indice12.png', 0, 0, 20),
(13, 'Code', 'data/objets/icone0.png', 'data/objets/indice13.png', 0, 1, 21),
(14, 'Livre clé', 'data/objets/icone14.png', 'data/objets/indice14.png', 0, 1, 24);

-- --------------------------------------------------------

--
-- Structure de la table `Outils`
--

CREATE TABLE `Outils` (
  `tid` int(11) NOT NULL,
  `t_nom` text DEFAULT NULL,
  `icone` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `charge` tinyint(1) NOT NULL DEFAULT 0,
  `estdebloquepar` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Outils`
--

INSERT INTO `Outils` (`tid`, `t_nom`, `icone`, `image`, `charge`, `estdebloquepar`) VALUES
(1, 'metro_londre', 'data/outils/iconemetro.png', 'data/outils/metro.png', 1, 0),
(2, 'cles', 'data/outils/iconecles.png', 'data/outils/cles.png', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `Personnages`
--

CREATE TABLE `Personnages` (
  `pid` int(11) NOT NULL,
  `p_nom` text DEFAULT NULL,
  `iconemap` text DEFAULT NULL,
  `imagefiche` text DEFAULT NULL,
  `textefiche` text DEFAULT NULL,
  `fichedebloquepar` int(11) DEFAULT NULL,
  `charge` tinyint(1) NOT NULL DEFAULT 0,
  `role` enum('temoin','victime','suspect','coupable') DEFAULT NULL,
  `explication` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `Personnages`
--

INSERT INTO `Personnages` (`pid`, `p_nom`, `iconemap`, `imagefiche`, `textefiche`, `fichedebloquepar`, `charge`, `role`, `explication`) VALUES
(1, 'Sebastian Wilkes', 'data/personnages/icone1.png', 'Nouveau PDG de la Banque. Vous le connaissez depuis longtemps, vous avez été à l\'université ensemble.', NULL, 1, 0, NULL, 2),
(2, 'Edward Van Coon', 'data/personnages/icone2.png', 'Chef du département des affaires de Hong Kong de la Banque. \r\nIl a étudié à Oxford et est doté d’un sang-froid sans pareil. \r\n<br>\r\n<i>Adresse</i> : 15 Lord North St, Westminster, London ', NULL, 2, 0, NULL, NULL),
(3, 'Brian Lukis', 'data/personnages/icone3.png', 'Grand reporter pour The Times. Il n’est pas très bavard mais très reconnu pour la qualité de ses articles sur l’Asie. \r\n<br>\r\n<i>Adresse</i> : 1 Golding St, Whitechapel, London \r\n\r\n \r\n ', NULL, 3, 0, NULL, NULL),
(4, 'Amanda', 'data/personnages/icone4.png', 'Assistante et secrétaire de M. Van Coon. Elle gère notamment son agenda et ses voyages. ', NULL, 6, 0, NULL, 11),
(5, 'Tom', 'data/personnages/icone5.png', 'Artiste graffeur. C’est un jeune homme joyeux et plein de ressources qui vous a déjà aidé dans plusieurs enquêtes. ', NULL, 9, 0, NULL, 21),
(6, 'Soo Lin Yao', 'data/personnages/icone6.png', 'Habite juste à côté du Lucky Cat. Elle est plutôt discrète et travaille au National Museum Antiquity. ', NULL, 9, 0, NULL, 22),
(7, 'Zhi Zhu', 'data/personnages/icone7.png', 'Assassin acrobate. Il est un membre très actif du Lotus Noir et est d’une discrétion redoutable. C’est le frère de Soo Lin Yao. ', NULL, 9, 0, NULL, NULL),
(8, 'Général Shan', 'data/personnages/icone8.png', 'Chef du Lotus Noir.', NULL, 10, 0, NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Explications`
--
ALTER TABLE `Explications`
  ADD PRIMARY KEY (`eid`);

--
-- Index pour la table `Images`
--
ALTER TABLE `Images`
  ADD PRIMARY KEY (`imid`);

--
-- Index pour la table `Joueurs`
--
ALTER TABLE `Joueurs`
  ADD PRIMARY KEY (`jid`);

--
-- Index pour la table `Lieux`
--
ALTER TABLE `Lieux`
  ADD PRIMARY KEY (`lid`);

--
-- Index pour la table `Message`
--
ALTER TABLE `Message`
  ADD PRIMARY KEY (`mid`);

--
-- Index pour la table `Objets`
--
ALTER TABLE `Objets`
  ADD PRIMARY KEY (`oid`);

--
-- Index pour la table `Outils`
--
ALTER TABLE `Outils`
  ADD PRIMARY KEY (`tid`);

--
-- Index pour la table `Personnages`
--
ALTER TABLE `Personnages`
  ADD PRIMARY KEY (`pid`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Explications`
--
ALTER TABLE `Explications`
  MODIFY `eid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `Joueurs`
--
ALTER TABLE `Joueurs`
  MODIFY `jid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT pour la table `Lieux`
--
ALTER TABLE `Lieux`
  MODIFY `lid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `Message`
--
ALTER TABLE `Message`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `Objets`
--
ALTER TABLE `Objets`
  MODIFY `oid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `Outils`
--
ALTER TABLE `Outils`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `Personnages`
--
ALTER TABLE `Personnages`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
