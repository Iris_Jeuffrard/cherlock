<?php

    include('connect.php');

    if (isset($_POST["Mscores"])) {

      $sql = "SELECT * FROM `Joueurs` ORDER BY 'score' DESC LIMIT 3";
      $result = mysqli_query($link, $sql);

      if (mysqli_num_rows($result)>0) {
        while ($ligne = mysqli_fetch_assoc($result)) {
          $joue[] = $ligne;
        }
      }

      if (isset($joue)) {
        echo json_encode($joue);
      }
   }


    if(isset($_POST["pseudo"])){

      $pseudo=$_POST["pseudo"];
      // $sql = "SELECT * FROM Joueurs WHERE pseudo='iris'";
      $sql = "SELECT * FROM `Joueurs` WHERE `pseudo`='".$_POST["pseudo"];
      $sql = $sql."'";
      $result = mysqli_query($link, $sql);

      if (mysqli_num_rows($result)>0) {
        while ($ligne = mysqli_fetch_assoc($result)) {
          $joue[] = $ligne;
        }
      }

      if (isset($joue)) {

        if ($joue[0]['score']==0) {
          $msg = "Heureux de te revoir ".$joue[0]['pseudo'].",\n\nj'espère que tu es prêt(e) à entrer dans la légende !";
          echo $msg;
        } elseif ($joue[0]['score']==1) {
          // $msg = "Bienvenue iris, venez retenter votre chance !";
          $msg = "Heureux de te revoir ".$joue[0]['pseudo']." !\n\nTu n'as pas réussi(e) à terminer à temps la dernière fois...\n\nMais viens retenter ta chance !";
          echo $msg;
        } else {
          $msg = "Heureux de te revoir ".$joue[0]['pseudo'].",\ntu t'en es bien sorti(e) la dernière fois !\n\nTu as mis ".intval(substr($joue[0]['temps'],0,2))." minutes et ".intval(substr($joue[0]['temps'],3,5))." secondes, ton score était de ".$joue[0]['score']." points.\n\nMais peut-être peux-tu encore t'améliorer ;)";
          echo $msg;
        }

       } else {
         // $ajoute_rq = "INSERT INTO Joueurs(pseudo) VALUES('iris')";
         $nom = $_POST['pseudo'];
         // $ajoute_rq = "INSERT INTO Joueurs(pseudo) VALUES('".$_POST['pseudo']."')";
         $ajoute_rq = 'INSERT INTO Joueurs(pseudo) VALUES()'.$nom.'")';
         $ajoute_rq = str_replace('VALUES()','VALUES("',$ajoute_rq);
         $ajoute_j = mysqli_query($link,$ajoute_rq);

         $msg = "Bienvenue ".$_POST["pseudo"].",\n\nj'espère que tu es prêt(e) à entrer dans la légende !";
         // $msg = "Bienvenue iris, j'espère que tu es prêt(e) à entrer dans la légende !";
         echo $msg;
       }
     }

     if(isset($_POST["temps"])){
       $reste_tps = substr($_POST["temps"],0,7);
       $pseudo = substr($_POST["temps"],7);

       $sql = "SELECT * FROM `Joueurs` WHERE `pseudo`='".$pseudo;
       $sql = $sql."'";
       $result = mysqli_query($link, $sql);


         while ($ligne = mysqli_fetch_assoc($result)) {
           $joue[] = $ligne;
         }




       if ($reste_tps[6]==1) {    //"00:00-1pseudo" lorsque le temps à été écoulé sans que le joueur finisse

         if ($joue[0]['score']<=1) {
           $set_sc = "UPDATE Joueurs SET score=1, temps='20:00' WHERE Joueurs.pseudo='".$pseudo."'";
           mysqli_query($link, $set_sc);
           echo "Le temps est malheureusement écoulé, ton score est de 1 point.";
         } else {
           echo "Le temps est malheureusement écoulé,\nton score sur cette partie est de 1 point.\n\nTu avais fait mieux la dernière fois,\nnous allons donc garder ton score précédent ;)";
         }

       } else {
         $min = 19-intval(substr($reste_tps,0,2));
         $sec = 59-intval(substr($reste_tps,3,5));

         if ($sec<10) {
           $tps_tot = strval('0' + $min + ':0' + $sec);
           if ($min>=10){
             $tps_tot = strval($min + ':0' + $sec);
           }
         } else {
           $tps_tot = strval('0'.$min.':'.$sec);
           if ($min>=10){
             $tps_tot = strval($min + ':' + $sec);
           }
         }
         $score = 10+intval(substr($reste_tps,0,2))*60+intval(substr($reste_tps,3,5)); //10+nb de temps restant en seconde

         if ($joue[0]['score']<$score) { //il a fait mieux
           $set_sc = "UPDATE Joueurs SET score=".$score.", temps='".$tps_tot."' WHERE Joueurs.pseudo='".$pseudo."'";
           mysqli_query($link, $set_sc);
           echo "Bien joué, tu as mis ".$min." minutes et ".$sec." secondes, ton score est de ".$score." points.\n\nRecord battu !!";
         } else {
           echo "Bien joué, tu as mis ".$min." minutes et ".$sec." secondes,\nton score est de ".$score." points.\n\nCependant, tu avais fait mieux la dernière fois,\nnous allons donc garder ton score précédent ;)";
         }

       }
     }
?>
