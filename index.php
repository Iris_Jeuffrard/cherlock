<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <!-- Own CSS -->
        <link rel="stylesheet" href="accueil.css">

        <title>Escape Game</title>
    </head>
    <body>
        <header>
            <h1>Elémentaire mon cher Lock !</h1>
        </header>
        <div class="container">

            <div class="row all-box">
                <div class="col-sm box box-form">
                    <div class="row">
                        <div class="col-12">
                            <h2>Bienvenue !</h2>
                            <h6>Te voilà dans la peau de Sherlock Holmes, il faudra t'armer de patience et de flair pour venir à bout de cette enquête...<br> Le sénario de l'enquête est inspiré de l'épisode 1 - saison 1 de la série Sherlock Holmes.<br><br></h6>
                        </div>
                        <div class="col-12">
                            <form action="game.php" method="post">
                                <div class="mb-3">
                                    <label for="inputPsuedo" class="form-label">Saisis ton pseudo pour jouer :</label>
                                    <input type="text" class="form-control" id="inputPseudo" name="pseudo" aria-describedby="pseudoHelp" placeholder="pseudo">
                                    <div id="pseudoHelp" class="form-text">
                                        Si tu viens rejouer, saisis le même pseudo que la dernière fois pour améliorer ton score <br>
                                        <?php
                                            if(isset($_GET['msg'])){
                                                if($_GET['msg']=='error1'){
                                                    echo "<b>Vous devez renseigner un pseudo avant de jouer !</b>";
                                                }elseif($_GET['msg']=='error2'){
                                                    echo "<b>la variable n'existe pas pour je ne sais pas quelle raison</b>";
                                                }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-lg">Jouer</button>
                            </form>
                        </div>
                    </div>

                </div>
                <div class="col-sm box box-podium">
                    <div class="col-12">
                        <h2>Meilleurs scores</h2>

                    </div>
                    <div class="col-12">
                        <div class="row align-items-end podium">
                            <?php
                                include('connect.php');

                                $sql = "SELECT * FROM `Joueurs`  WHERE `Joueurs`.`score` is not NULL ORDER BY `Joueurs`.`score` DESC LIMIT 3";
                                $result = mysqli_query($link, $sql);

                                if (mysqli_num_rows($result)>0) {
                                    $i = 1;
                                    while ($ligne = mysqli_fetch_assoc($result)) {
                                    echo "<div class='col-sm pod first-pod'><p class='medaille medaille-".$i."'>".$i."</p>".$ligne['pseudo']."<br>Score = ".$ligne['score']."<br>".$ligne['temps']."</div>";
                                    $i = $i + 1;
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

        <footer>
            Escape Game réalisé par Iris Jeuffrard &amp; Mathilde Waymel - Novembre 2020
        </footer>



</body>
</html>
