window.onload = function(){

    var mymap = L.map('mapid').setView([51.523763, -0.158501], 15);

    var ifrm = window.frameElement;
    var doc = ifrm.ownerDocument;

    //Pour afficher les meilleurs scores
    var sc1 = doc.getElementById("sc1");
    var sc2 = doc.getElementById("sc2");
    var sc3 = doc.getElementById("sc3");

    //Pour gérer le temps
    var debut;
    var heure_debut;
    var minutes;
    var secondes;
    var decompte = doc.getElementById('chrono');
    var message_tps = doc.getElementById('message_temps');

    var jeu_fini=0;


    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mymap);

    requeteJoueur();
    main();

    async function main(){
        /**
         * recupere les infos du premier lieu chargé & les premiers message et outils du détective
         * affiche l'icone et son contenu (popup) du premier lieu selon son niveau de zoom
         * ajoute le lieu a la liste des lieux deja visités
         * ajoute un listener sur ce lieu pour déclencher le suivant
         */

        // recupere les infos du premier lieu chargé & les premiers message et outils du détective
        var lieuDepart = await requeteInitLieu();
        var MessageDepart = await requeteInitMessage();
        var OutilDepart = await requeteInitOutil();
        debloqueMessage(MessageDepart);
        debloqueOutil(OutilDepart[0]);
        debloqueOutil(OutilDepart[1]);
        afficherOutils(OutilDepart[0].tid)
        afficherOutils(OutilDepart[1].tid)



        // Affichage icone du premier lieu
        var image = await requeteImage(lieuDepart.icone);
        var Icon = L.icon({
            iconUrl: image.chemin,
            iconSize:     [image.largeur, image.longeur], // size of the icon
            iconAnchor:   [image.ancre_x, image.ancre_y], // point of the icon which will correspond to marker's location
            popupAnchor:  [0, -20] // point from which the popup should open relative to the iconAnchor
        });

        var marker = L.marker([lieuDepart.latitude,lieuDepart.longitude],{icon: Icon, attribution: lieuDepart.zoom}).addTo(mymap);
        var LayersMarker = new L.FeatureGroup();
        LayersMarker.addLayer(marker);

        // Affiche markers selon le niveau de zooom
        mymap.on('zoomend', function(){
            LayersMarker.eachLayer(function (layer) {
                zoomMin = layer.getAttribution();
                if (mymap.getZoom() < zoomMin){
                    mymap.removeLayer(layer);
                }
                else {
                    mymap.addLayer(layer);
                }
            });
        });

        // Affichage contenu du premier lieu dans un popup
        var content = await definirContentLieu(lieuDepart);
        marker.bindPopup(content)

        // accionne le marker du lieu suivant
        var lid = lieuDepart.lid;
        var lieuxVisites = [lid]; // liste des lieux deja visites
        marker.addEventListener('click', function() {enchaine(lieuxVisites,LayersMarker,lieuDepart.lid)});
    }

    async function enchaine(lieuxVisites,LayersMarker,idbloquant){
        /**
         * recupere infos lieu debloque
         * si le lieu n'a pas été déja visité :
         * débloque les fiches personnage, les messages et les outils associés au lieu en cours
         *   si le lieu existe physiquement :
         *   ajoute le lieu a la liste des lieux deja visités
         *   ajoute l'option boussole pour retrouver le lieu debloque
         *   ajoute un listener pour pouvoir récuperer les indices du lieu en cours
         *   affiche l'icone et son contenu (popup) du lieu debloque selon son niveau de zoom
         *   ajoute un listener sur ce lieu pour rappeler cette fonction
         *   sinon :
         *   ajoute le lieu a la liste des lieux deja visités
         *   ajoute les infos dans une modal bootstrap dans la carte
         *   rappelle la fonction
         * si dernier lieu : fin du jeu
         *
         * @param lieuxVisites: liste des lieux deja visité (1 clique)
         * @param LayersMarker: groupe leaflet de couche de marker
         * @param idbloquant: id du lieu en cours (sur lequel l'utilisateur a cliqué)
         */

        var lieuDebloque = await requeteLieu(idbloquant); // recupere infos lieu debloque

        //console.log('lieubloquant '+idbloquant+' lieudebloque '+lieuDebloque.lid);
        if (typeof lieuDebloque !=='undefined' && !(lieuxVisites.includes(lieuDebloque.lid))){ // condition d'arret ou lieu déjà visité
            // Debloquage des fiches personnages
            debloqueFiche(idbloquant)

            // Debloque les messages et outils du lieu
            var Message = await requeteMessage(idbloquant);
            debloqueMessage(Message);

            if(lieuDebloque.latitude != null){
                lieuxVisites.push(lieuDebloque.lid);

                // Option boussole pour retrouver son chemin :)
                var ifrm = window.frameElement;
                var doc = ifrm.ownerDocument;
                var bouss = doc.getElementById("divBoussole");
                bouss.addEventListener('click', function() {boussole(lieuDebloque,event)});

                // Event sur l'icone d'un indice
                var iconesIndices = document.getElementsByClassName("indice");
                for(var k=0;k<iconesIndices.length;k++){
                    var iconeIndice = iconesIndices[k];
                    iconeIndice.addEventListener('click', function() {recupererIndice(event)});
                }

                // Creation marker avec affichage icone du lieu debloque
                var image = await requeteImage(lieuDebloque.icone);
                var Icon = L.icon({
                    iconUrl: image.chemin,
                    iconSize:     [image.largeur, image.longeur], // size of the icon
                    iconAnchor:   [image.ancre_x, image.ancre_y], // point of the icon which will correspond to marker's location
                    popupAnchor:  [0, -20] // point from which the popup should open relative to the iconAnchor
                });

                var marker = L.marker([lieuDebloque.latitude,lieuDebloque.longitude],{icon: Icon, attribution: lieuDebloque.zoom}).addTo(mymap);
                LayersMarker.addLayer(marker);

                // Affiche markers selon le niveau de zooom
                mymap.on('zoomend', function(){
                    LayersMarker.eachLayer(function (layer) {
                        zoomMin = layer.getAttribution();
                        if (mymap.getZoom() < zoomMin){
                            mymap.removeLayer(layer);
                        }
                        else {
                            mymap.addLayer(layer);
                        }
                    });
                });

                // Affichage du contenu du lieu debloque
                var content = await definirContentLieu(lieuDebloque);
                marker.bindPopup(content)

                // accionne le marker du lieu debloque
                marker.addEventListener('click', function() {enchaine(lieuxVisites,LayersMarker,lieuDebloque.lid)});

            }else{ // cas des lieux abstrait de la bdd (maison de la presse)
                lieuxVisites.push(lieuDebloque.lid);

                // Cree les infos dans une modal dans la carte
                var explication = await requeteExplication(lieuDebloque.explication);
                var objets = await requeteObjet(explication.eid);
                var obj = objets[0]
                var content = "<p><img src="+obj.image+" alt='indice du lieu actuel' style='height: 100%; width: 100%; object-fit: contain'></p>";
                document.getElementById("ModalLabel").innerHTML = explication.e_nom;
                document.getElementById("modal-content-div").innerHTML = content;

                enchaine(lieuxVisites,LayersMarker,lieuDebloque.lid);
            }
        }else if (typeof lieuDebloque =='undefined'){
            setTimeout(fin_jeu(), 3000);
        }
    }

    // Fonctions annexes

    async function definirContentLieu(lieu){
        /**
         * requete pour connaitre l'explication associée au lieu
         * affiche le contenu de l'explication dans un tab bootstrap
         * tant qu'il y a une explication suivante dans ce lieu, l'ajouter au tab
         *
         * @param lieu: objet lieu
         * @return :  contenu à afficher dans le popup du lieu
         */

        var explication = await requeteExplication(lieu.explication);
        var contentExplication = await definirContentExplication(explication);

        if(explication.fin==1){
            var pills = ""
        }else{
            var pills = '<li class="nav-item" role="presentation">'
                            +'<a class="nav-link active" id="pills-'+explication.eid+'-tab" data-toggle="pill" href="#pills-'+explication.eid+'" role="tab" aria-controls="pills-'+explication.eid+'" aria-selected="true">'+explication.e_nom+'</a>'
                        +'</li>'
        }
        var panes = '<div class="tab-pane fade show active" id="pills-'+explication.eid+'" role="tabpanel" aria-labelledby="pills-'+explication.eid+'-tab">'+contentExplication+'</div>'

        var fin = explication.fin;
        var idsuivante = explication.esuivante
        while(fin==0){
            var explicationSuivante = await requeteExplication(idsuivante);
            var contentExplication = await definirContentExplication(explicationSuivante);
            var classname = "nav-link"
            if(explicationSuivante.displayed==1){
                classname = "nav-link disabled"
            }
            pills +=  '<li class="nav-item" role="presentation">'
                             +'<a class="'+classname+'" id="pills-'+explicationSuivante.eid+'-tab" data-toggle="pill" href="#pills-'+explicationSuivante.eid+'" role="tab" aria-controls="pills-'+explicationSuivante.eid+'" aria-selected="false">'+explicationSuivante.e_nom+'</a>'
                         +'</li>'
            panes += '<div class="tab-pane fade" id="pills-'+explicationSuivante.eid+'" role="tabpanel" aria-labelledby="pills-'+explicationSuivante.eid+'-tab">'+contentExplication+'</div>'

            fin = explicationSuivante.fin;
            idsuivante = explicationSuivante.esuivante;
        }
        var content = '<div class="tab-content" id="pills-tabContent">'
                        + panes
                    + '</div>'
                    + '<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">'
                        + pills
                    + '</ul>';

        return content
    }

    async function definirContentExplication(explication){
        /**
         * requete pour connaitre les persos associés à l'explication
         * requete pour connaitre les objets associés à l'explication
         * adapte les donnees en contenu html
         *
         * @param lieu: objet explication
         * @return: contenu de l'explication (texte+perso+objet)
         */

        var objets = await requeteObjet(explication.eid);
        var perso = await requetePerso(explication.eid);
        var imgPerso="";
        if(typeof perso !== 'undefined'){
            imgPerso = "<img src="+perso.iconemap+" alt='perso du lieu actuel' width='50' height='50'>";
        }
        var imgObjet="";
        if(typeof objets !== 'undefined'){
            for(var k=0;k<objets.length;k++) {
                var obj = objets[k];
                imgObjet += "<img class='indice' id='indice_"+obj.oid+"' src="+obj.iconemap+" data-image='"+obj.image+"' data-recuperable='"+obj.recuperable+"' alt='indice du lieu actuel' style='object-fit: contain'>";
            }
        }
        return explication.content+'<p>'+imgPerso+imgObjet+'</p>'
    }

    function recupererIndice(event){
        /**
         * si l'objet cible de l'event est recuperable, le deplacer ds la div indices collectes
         * afficher l'objet
         *
         * @param event: objet event
         */

        data_recup = event.target.getAttribute('data-recuperable');
        if (data_recup==1){
            var ifrm = window.frameElement;
            var doc = ifrm.ownerDocument;
            var packIndices = doc.getElementsByClassName("toutIndices");
            packIndices[0].appendChild(event.target);
        }
        data_img = event.target.getAttribute('data-image');
        afficherIndice(data_img);
    }

    function afficherIndice(data_img){
        /**
         * afficher l'image dans une modal bootstrap
         *
         * @param data_img: chemin de l'image
         */

         var ifrm = window.frameElement;
        var doc = ifrm.ownerDocument;
        doc.getElementById("ModalLabel").innerHTML = "Visualisation de l'indice";
        doc.getElementById("modal-content-div").innerHTML = "<img src='"+data_img+"' alt='indice récupéré' style='height: 100%; width: 100%; object-fit: contain'></p>";
        var modalGame = doc.getElementById("modalGame");
        $(modalGame).modal('show')
    }

    async function debloqueFiche(id){
        /**
         * si le lieu debloque une fiche perso (requete), ajoute le contenue dans sa div
         *
         * @param id: id du lieu
         */

         var fiches = await requeteFichePerso(id);
        if(typeof fiches !== 'undefined'){
            for(var k=0;k<fiches.length;k++) {
                var fiche = fiches[k];
                var ifrm = window.frameElement;
                var doc = ifrm.ownerDocument;
                doc.getElementById("p_"+fiche.pid).innerHTML = "<b>"+fiche.p_nom+"</b><br><img src="+fiche.iconemap+" width='60' height='60' style='float: left;'><p style='display: inline;'>"+fiche.imagefiche+"</p>";
            }
        }
    }

    async function debloqueMessage(message){
        /**
         * si le message existe, ajoute le contenu dans sa div
         *
         * @param message: objet message
         */

         if(typeof message !== 'undefined'){
            var ifrm = window.frameElement;
            var doc = ifrm.ownerDocument;
            doc.getElementById("message").innerHTML = message.message;
        }
    }

    async function debloqueOutil(outil){
        /**
         * affiche icone de l'outil dans la div outil collectes
         *
         * @param outil: objet outil
         */

        //Affiche l'icone
        var ifrm = window.frameElement;
        var doc = ifrm.ownerDocument;
        doc.getElementById("outil").innerHTML += "<img id='outil_"+outil.tid+"' src='"+outil.icone+"' alt='icone outil récupéré' data-image='"+outil.image+"' style= object-fit: contain'> ";
    }

    function afficherOutils(id){
        /**
         * ajoute un event click sur l'icone de l'outil pour l'ouvrir dans une modal
         *
         * @param outil: objet outil
         */

        //Ouvre une modal qd click
        var icone = doc.getElementById("outil_"+id);
        icone.addEventListener('click', function(event) {
            data_img = event.target.getAttribute('data-image');
            var ifrm = window.frameElement;
            var doc = ifrm.ownerDocument;
            doc.getElementById("ModalLabel").innerHTML = "Visualisation de l'outil";
            doc.getElementById("modal-content-div").innerHTML = "<img src='"+data_img+"' alt='outil récupéré' style='height: 100%; width: 100%; object-fit: contain'></p>";
            var modalGame = doc.getElementById("modalGame");
            $(modalGame).modal('show');
        });
    }

    function boussole(lieuATrouver,event){
        /**
         * recupere les coord du centre de la map
         * calcule l'orientation du lieu à trouver par rapport à ce centre (en degre)
         * affiche la fleche de la boussole en appliquant la rotation calculé
         *
         * @param lieuATrouver: objet lieu
         */

        // Calcule orientation du lieu
        var coordCenterMap = mymap.getCenter();
        var dlat = lieuATrouver.latitude - coordCenterMap.lat;
        var dlng = lieuATrouver.longitude - coordCenterMap.lng;;
        var N = Math.sqrt(dlng*dlng+dlat*dlat)
        var rotation = 2*Math.atan(dlng/(dlat+N))*180/Math.PI;
        //Affiche fleche directionnelle
        var ifrm = window.frameElement;
        var doc = ifrm.ownerDocument;
        doc.getElementById("divBoussole").innerHTML = "<img src='data/outils/fleche.png' alt='fleche boussole' style='transform: rotate("+rotation+"deg); position:absolute; top: 400px; left:50px; object-fit: contain'>";
    }

    // Fonctions requetes au PHP

    async function requeteInitLieu(){
        try{
            var data = "initLieu=peuimporte";

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r[0];
        }catch(error){
            console.error(error);
        }
    }

    async function requeteInitMessage(){
        try{
            var data = "initMessage=peuimporte";

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r[0];
        }catch(error){
            console.error(error);
        }
    }

    async function requeteInitOutil(){
        try{
            var data = "initOutil=peuimporte";

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r;
        }catch(error){
            console.error(error);
        }
    }

    async function requeteLieu(id){
        try{
            var data = "bloqueur="+id;

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r[0];
        }catch(error){
            console.error(error);
        }
    }

    async function requeteImage(id){
        try{
            var data = "idimage="+id;

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r[0];
        }catch(error){
            console.error(error);
        }
    }

    async function requeteExplication(id){
        try{
            var data = "idExplication="+id;

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r[0];
        }catch(error){
            console.error(error);
        }
    }

    async function requeteObjet(id){
        try{
            var data = "explicationContenantObjet="+id;

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r;
        }catch(error){
            console.error(error);
        }
    }

    async function requetePerso(id){
        try{
            var data = "explicationContenantPerso="+id;

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r[0];
        }catch(error){
            console.error(error);
        }
    }

    async function requeteFichePerso(id){
        try{
            var data = "lieuDebloquantFichePerso="+id;

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r;
        }catch(error){
            console.error(error);
        }
    }

    async function requeteMessage(id){
        try{
            var data = "lieuDebloquantMessage="+id;

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r[0];
        }catch(error){
            console.error(error);
        }
    }

    async function requeteOutil(id){
        try{
            var data = "lieuDebloquantOutil="+id;

            const response = await fetch('bdd.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.json();
            return r[0];
        }catch(error){
            console.error(error);
        }
    }


    // Fonctions qui gère les pseudos, meilleurs scores et le temps

    async function afficheMeilleursSc(){
        /**
         * Recupere le pseudo du joueur, cree le joueur dans la base de donnée et lance le chrono
         */
      try{
            var data = "Mscores="+3;

            const response = await fetch('pseudo.php', {
                    method: 'post',
                    body: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    })
            const r = await response.json();
            sc1.innerHTML = r[0]['pseudo']+"<Br>Score : "+r[0]['score']+"<Br>"+r[0]['temps'];
            sc2.innerHTML = r[1]['pseudo']+"<Br>Score : "+r[1]['score']+"<Br>"+r[1]['temps'];
            sc3.innerHTML = r[2]['pseudo']+"<Br>Score : "+r[2]['score']+"<Br>"+r[2]['temps'];

      }catch(error){
            console.error(error);
      }
    }

    async function requeteJoueur(){
        /**
         * Recupere le pseudo du joueur, cree le joueur dans la base de donnée et lance le chrono
         */

        var ifrm = window.frameElement;
        var doc = ifrm.ownerDocument;
        var pseudo_j = doc.getElementById("pseudo").textContent;

        try{
            var data = "pseudo="+pseudo_j;

            const response = await fetch('pseudo.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })

            const content = await response.text();

            var ifrm = window.frameElement;
            var doc = ifrm.ownerDocument;
            doc.getElementById("ModalLabel").innerHTML = "C'est Parti !";
            doc.getElementById("modal-content-div").innerHTML = content+"<br><br> Tout au long du jeu, des messages sont là pour t'aider, ne les oublies pas ;) <br> ⬅";
            var modalGame = doc.getElementById("modalGame");
            $(modalGame).modal('show');

            debut = new Date();
            heure_debut = debut.toLocaleTimeString('fr');
            afficheMeilleursSc();
            loop();

        }catch(error){
            console.error(error);
        }

    }

    async function fin_jeu(temps_depasse=0) {
        /**
         * Arrete le jeu, récupère le temps du joueur
         * Met à jour la base de données avec le temps et le score du joueur
         * Indique au joueur son score et son temps et recharge la page
         *
         * @param temps_depasse: boolean 1 sur le joueur n'a pas reussi à finir pendant le temps imparti, 0 s'il a terminé dans les temps
         */

        try{
            var temps_j;
            var ifrm = window.frameElement;
            var doc = ifrm.ownerDocument;
            var pseudo_j = doc.getElementById("pseudo").textContent;

            if (minutes<0) {
                minutes=0;
                secondes=0;
            }

            if (secondes<10) {
              temps_j = '0' + minutes + ':0' + secondes;
                if (minutes>=10){
                    temps_j = minutes + ':0' + secondes;
                }
            } else {
              temps_j = '0' + minutes + ':' + secondes;
              if (minutes>=10){
                  temps_j = minutes + ':' + secondes;
              }
            }

            var data = "temps="+temps_j+'-'+temps_depasse+pseudo_j;

            const response = await fetch('pseudo.php', {
                method: 'post',
                body: data,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
            const r = await response.text();
            jeu_fini=1;

            var btnRejouer = '<a href="index.php" class="btn btn-success active" role="button" aria-pressed="true">Rejouer</a>';
            var content = r+"<br><br>Nous espérons que tu t'es bien amusé(e) !<br>N'hésite pas à recommencer pour tenter d'améliorer ton score ;)<br><br>A bientôt !<br><br>"+btnRejouer;

            var ifrm = window.frameElement;
            var doc = ifrm.ownerDocument;
            doc.getElementById("ModalLabel").innerHTML = "C'est fini !";
            doc.getElementById("modal-content-div").innerHTML = content;
            var modalGame = doc.getElementById("modalGame");
            $(modalGame).modal('show');

            }catch(error){
            console.error(error);
        }
    }

    function loop(){
        /**
         * Gère et affiche le temps restant au joueur (décompte)
         * si le temps est écoulé, arrète le jeu
         */
        var date = new Date();
        var heure = date.toLocaleTimeString('fr');
        var decompte_seconde = (debut-date)/1000+20*60;
        minutes = Math.floor(decompte_seconde/ 60);
        secondes = Math.floor(decompte_seconde - (minutes * 60));

        if (secondes<10) {
        decompte.innerHTML = '0' + minutes + ':0' + secondes;
          if (minutes>=10){
            decompte.innerHTML = minutes + ':0' + secondes;
          }
        } else {
        decompte.innerHTML = '0' + minutes + ':' + secondes;
          if (minutes>=10){
            decompte.innerHTML = minutes + ':' + secondes;
          }
        }

        if(secondes == 0){
            decompte.style.color = 'red';
            decompte.style.fontWeight = 'bold';
        }else if(minutes == 0){
            decompte.style.color = 'red';
            decompte.style.fontWeight = 'bold';
        }else if(minutes < 0){
            decompte.innerHTML = '00:00';
            decompte.style.color = 'red';
            decompte.style.fontWeight = 'bold';
            message_tps.innerHTML = 'Temps écoulé'

        }else{
            decompte.style.color = '';
            decompte.style.fontWeight = '';
        }
        if (minutes >= 0) {
          if (jeu_fini==0) {
            setTimeout(loop, 1000);
          }
        } else {
          fin_jeu(1);
        }
    }
 }
